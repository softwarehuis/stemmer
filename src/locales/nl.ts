import  en from "./en"

const nl : typeof en = {
    welcome: 'Welkomsttekst',
    welcome_paragraph: 'Welkomstparagraaf',
    copyright: '©2023 [SiteNaam]. Alle rechten voorbehouden.',
}

export default nl