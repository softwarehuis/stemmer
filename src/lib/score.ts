import { writable } from "svelte/store";
import type { Score } from "./types";

export const score = writable<Score[]>([]);