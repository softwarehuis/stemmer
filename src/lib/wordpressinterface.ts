import { writable } from "svelte/store";
import WPAPI from "wpapi";
import { endpoint } from "../settings";
import { loading } from "./store";

var wp = new WPAPI({
    endpoint: endpoint
});
wp.stands = wp.registerRoute('wp/v2', '/standpunt/(?P<id>)')
wp.pillars = wp.registerRoute('wp/v2', '/pijler/(?P<id>)')
export const post = writable<any>();


export const getStands = async (page: number = 1, pageSize: number = 10) => {
    const response: any = await wp.stands().perPage(pageSize).page(page)
    post.set(response)
}

export const getStand = async (id: number) => {
    const response: any = await wp.stands().id(id)
    post.set(response)
}

export const getPillars = async (page: number = 1, pageSize: number = 10) => {
    loading.set(true)
    const response: any = await wp.pillars().perPage(pageSize).page(page)
    post.set(response)
    loading.set(false)
    
}

export const getPillar = async (id: number) => {
    loading.set(true)
    const response: any = await wp.pillars().id(id)
    post.set(response)
    loading.set(false)
}