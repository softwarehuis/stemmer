import { writable } from "svelte/store";
import type { Party, Question } from "./types";

export const loading = writable<boolean>(false);
export const currentQuestion = writable<string>("");
export const parties = writable<Party[]>([]);
export const questions = writable<Question[]>([]);

const ApiGet = async (url: URL, parameters: any) => {
    try {
        // Map the parameters to request Parameters ommit null and undefined
        if (parameters) {
            Object.keys(parameters).forEach(key => url.searchParams.append(key, parameters[key]))
        }

        // Perform fetch
        const result = await fetch(url.toString().replaceAll("+", "%20"), {
            mode: "cors",
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }).then((res) => res.json());
        if (result.errors) {
            console.error({ errors: result.errors });
        } else if (!result) {
            console.error({ result });
            return 'No results found.';
        }
        return result;
    } catch (error) {
        console.error(error);
    }
}

export const getParties = async (parameters?: any) => {
    parties.set([])
    const response: any = await ApiGet(new URL("/data/parties.json", document.location.href), parameters)
    parties.set(response)
}

export const getQuestions = async (parameters?: any) => {
    parties.set([])
    const response: any = await ApiGet(new URL("/data/questions.json", document.location.href), parameters)
    questions.set(response)
}


