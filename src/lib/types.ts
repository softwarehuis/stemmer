export interface Party {
    id: number
    name: string
    full: string
}

export interface Question {
    id: string
    title: string
    pillar: number
}

export interface Score {
    id: number
}

export interface Post {
    id: number
    name: string
    description: string
}

export interface Stand {
    id: string
    title: string
    stand: number
    pillars: number[]
}
