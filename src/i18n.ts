import { addMessages, init, getLocaleFromNavigator } from 'svelte-i18n';
import { writable, get } from 'svelte/store';
import { fallbackLocale } from './settings';
import locales from './locales';

const detectedLocale: string = getLocaleFromNavigator() || "en";
const languages = [fallbackLocale, 'nl'];

export const language = writable<string>(fallbackLocale);

addMessages('en', locales['en']);
addMessages('nl', locales['nl']);


if (detectedLocale) {
    if (languages.indexOf(detectedLocale) > -1) language.set(detectedLocale);
    if (detectedLocale.indexOf('-') > 0) {
        const foundLng = languages.find((l) => detectedLocale.indexOf(l + '-') === 0);
        if (foundLng) language.set(foundLng);
    }
}

init({
    fallbackLocale,
    initialLocale: get(language)
});
