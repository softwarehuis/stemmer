""" Script to download from a wordpress website """
import json
from urllib.request import urlopen, Request

BASE_URL = "https://stemlp.nl/wp-json/wp/v2/"
STANDS = BASE_URL + "standpunt/?page=1&per_page=100"
PILLARS = BASE_URL + "pijler/?page=1&per_page=100"


def get_stands():
    """ Get stands data from the website """
    _result = []
    _data = get_data(STANDS)
    _i = 0
    for item in _data:
        _result.append({
            "id": f"{_i + 1}",
            "title": item["title"]["rendered"],
            "stand": item["id"],
            "pillars": item["pijler"]
            }
        )
        _i = _i + 1
    # Write to file
    with open('../public/data/stands.json', 'w', encoding='utf-8') as f:
        json.dump(_result, f, ensure_ascii=False, indent=2)


def get_pillars():
    """ Get pillars data from the website """
    _result = []
    _data = get_data(PILLARS)
    _i = 0
    for item in _data:
        _result.append({"id": f"{_i + 1}", "title": item["name"], "pillar": item["id"]})
        _i = _i + 1
    # Write to file
    with open('../public/data/questions.json', 'w', encoding='utf-8') as f:
        json.dump(_result, f, ensure_ascii=False, indent=2)


def get_data(url):
    """ Generic function to get data from url and return it as json """
    _request = Request(url,headers={'User-Agent': 'Mozilla/5.0'})
    with urlopen(_request) as _response:
        if _response.getcode() == 200:
            return json.loads(_response.read().decode('utf-8'))

    return []


def main():
    """ Main function """
    get_stands()
    get_pillars()


if __name__ == "__main__":
    main()
